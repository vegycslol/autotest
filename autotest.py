import string
from hypothesis import given as hypo_given
from hypothesis.searchstrategy.deferred import DeferredStrategy
from hypothesis.strategies import one_of as hypo_one_of, \
                                floats as hypo_floats, \
                                booleans as hypo_booleans, \
                                integers as hypo_integers, \
                                text as hypo_text, \
                                lists as hypo_lists, \
                                tuples as hypo_tuples, \
                                sets as hypo_sets, \
                                sampled_from, \
                                dictionaries

from cStringIO import StringIO
import sys
import inspect
import time
import traceback

# TODO: auto_test.register(fn, iterable of types)


_basictype_mapper = {
    int: hypo_integers,
    str: hypo_text,
    float: hypo_floats,
    bool: hypo_booleans,
    long: hypo_integers
}

_type_params = {
    hypo_text: {"alphabet": string.printable}
}


def _get_valid_types(type):
    """Gets a type/list/tuple of types and returns a tuple of types' instances
    example: [<type 'int'>, <type 'str'>] --> (IntType(), StringType())
    """
    if not hasattr(type, "__iter__"):
        # only 1 type return tuple with 1 element
        return (_basictype_mapper[type],)
    return tuple(map(lambda y: _basictype_mapper[y], type))


def _get_params_of_type(type):
    return _type_params.get(type, {})


def convert_to_hypo(arg):
    """Converts argument to hypothesis argument"""
    if isinstance(arg, DeferredStrategy):
        # got hypothesis argument, eg. sample_from
        return arg
    strategies = hypo_one_of(*map(lambda x: x(**_get_params_of_type(x)),
                                  _get_valid_types(arg)))
    if isinstance(arg, list):
        return hypo_lists(strategies)
    elif isinstance(arg, tuple):
        return hypo_tuples(strategies)
    elif isinstance(arg, set):
        return hypo_sets(strategies)
    else:
        return strategies


class auto_test:
    """This class is a decorator which saves all functions (which are decorated
    with it) together with their arguments as elements of dictionary where key
    is the name of a function and value is a tuple (func, args, kwargs).
    This dictionary is stored as a class attribute of a decorator named
    'functions'.
    """

    # class variable, dictionary with tuples (func, args, kwargs, error)
    # where error is a string describing what happened before testing
    functions = {}
    # list containing function names, here just so i dont use OrderedDict
    # which requires py 2.7 or more
    functions_names = []
    # results of tests, key = function's name,
    # value = (failed, fail_call, trace, error), where failed is True if
    # it failed on at least one test, fail_call is minified test, trace is
    # exception output and error is error that occurred before testing
    test_results = {}
    # testing statuses
    status = {
        0: "OK",
        1: "FAILED",
        2: "NOT TESTED"
    }

    def __init__(self, *args, **kwargs):
        # store decorators parameters
        self.args = args
        self.kwargs = kwargs
        self._update_kwargs()

    def __call__(self, func):
        error = None
        if len(self.args) != len(inspect.getargspec(func).args):
            error = ("decorator parameters don't match with "
                     "function's parameters")
        # save function, its parameters to class variable and error
        self.__class__.functions[func.__name__] = (func, self.args,
                                                   self.kwargs, error)
        self.__class__.functions_names.append(func.__name__)
        return func

    def _update_kwargs(self):
        if "rv" not in self.kwargs or self.kwargs["rv"] is None:
            self.kwargs["rv"] = [type(None)]  # default return value
        elif not isinstance(self.kwargs["rv"], list):
            self.kwargs["rv"] = [self.kwargs["rv"]]  # can't use list function
        # update kwargs with int/long and str/unicode
        if int in self.kwargs["rv"] or long in self.kwargs["rv"]:
            self.kwargs["rv"].extend([int, long])
        if str in self.kwargs["rv"] or unicode in self.kwargs["rv"]:
            self.kwargs["rv"].extend([str, unicode])
        # added duplicates, so remove them
        self.kwargs["rv"] = list(set(self.kwargs["rv"]))

    @classmethod
    def _generate_tests(cls, func, *args, **kwargs):
        hypo_args = [convert_to_hypo(x) for x in args]
        hypo_kwargs = {name: convert_to_hypo(arg_type) for name, arg_type in
                    kwargs.iteritems() if name != "rv"}
        return_type = kwargs.get("rv")  # None if key doesnt exist

        def _add_return_assert(f):
            args = inspect.getargspec(f).args
            # kwargs = inspect.getargspec(f).keywords
            defaults = inspect.getargspec(f).defaults
            args_wo_defaults, args_with_defaults = None, None
            if defaults is None:
                args_wo_defaults = [x for x in args]
                args_with_defaults = []
            else:
                args_wo_defaults = [x for x in args[:-len(defaults)]]
                args_with_defaults = [x + "=" + str(defaults[i]) for i, x in
                                    enumerate(args[-len(defaults):])]
            kwargs = ["**kwargs"]
            partial_args = ["_f", "_return_type"]
            args_str = ",".join(partial_args + args_wo_defaults +
                                args_with_defaults + kwargs)
            args_for_f_str = ",".join([x for x in args] + kwargs)
            fn_body = """
            def test_inner({0}):
                rv = _f({1})"""
            # separate so it doesnt conflict with inner format
            fn_body = fn_body.format(args_str, args_for_f_str)
            fn_body += """
                if type(rv) not in _return_type:
                    raise AssertionError("invalid return type, expected: {0}, \
got: {1}".format(' or'.join(map(str,_return_type)), type(rv)))
            """
            # create test_inner, will prepend function and return type l8r
            exec fn_body.strip() in \
                globals(), locals()
            test_inner.__name__ = f.__name__  # same as original function
            return test_inner

        # change stdout so it doesnt print to user
        old_stdout = sys.stdout
        sys.stdout = mystdout = StringIO()
        try:
            f = hypo_given(*hypo_args, **hypo_kwargs)(_add_return_assert(func))
            f(func, return_type)
            cls.test_results[func.__name__] = (False, None, None, None)
        except:
            trace = traceback.format_exc().strip()
            cls.test_results[func.__name__] = (True,
                                               mystdout.getvalue().strip(),
                                               trace,
                                               None)
        finally:
            sys.stdout = old_stdout

    @classmethod
    def runtests(cls, output=None):
        """Runs tests for all functions"""
        old_stdout = sys.stdout
        # check if we should write results to a file
        if output is not None:
                sys.stdout = open(output, "w")
        start_time = time.time()
        for _, (func, args, kwargs, error) in cls.functions.iteritems():
            if error is not None:
                # function should be skipped because error occurred before
                # testing phase
                cls.test_results[func.__name__] = (False, None, None, error)
                continue

            # run tests for this function
            cls._generate_tests(func, *args, **kwargs)
        # output testing info
        function_separator = "=" * 80  # separator between functions in report
        inner_separator = "-" * 80
        nr_errors, nr_skipped = 0, 0
        print "Testing functions: [{0}]".format(", ".join(cls.functions_names))
        for func_name in cls.functions_names:
            failed, hypo_output, trace, error = cls.test_results[func_name]
            if error is not None:
                print function_separator
                print "SKIPPED: {0}".format(func_name)
                print inner_separator
                print "Status: {0}\nReason: {1}\n" \
                    .format(cls.status[2], error)
                nr_skipped += 1
                continue

            status = cls.status[1] if failed else cls.status[0]
            if failed:
                test_case = hypo_output.split("Falsifying example: ")[1]
                correct_output = test_case.split("(")[0] + "(" + \
                    ",".join(test_case.split("(")[1].split(")")[0]
                             .split(",")[2:]).strip() + ")" + \
                    test_case.split(")")[1]
                print function_separator
                print "ERROR: {0}".format(func_name)
                print inner_separator
                print "Status: {0}\nFailing test: {1}\nReason: {2}\n" \
                    .format(status, correct_output, trace)
                nr_errors += 1
        print inner_separator
        print "OK" if nr_errors + nr_skipped == 0 else \
            "FAILED (skipped={0}, errors={1})".format(nr_skipped, nr_errors)
        print "Tested {0} functions in {1:.2f} seconds" \
            .format(len(cls.functions_names), time.time() - start_time)
        sys.stdout = old_stdout  # reset stdout


# example of decorator with a normal function
#
# def auto_registerer():
#     """returns a decorator and saves all functions (which are decorated with
#     this decorator) together with their arguments as elements of dictionary
#     where key is name of a function and value is a tuple (func, args, kwargs).
#     this dictionary is stored as attribute of a decorator named 'functions'.
#     """
#     funcs = {}

#     def auto_test(*args, **kwargs):
#         def register_fn(func):
#             funcs[func.__name__] = (func, args, kwargs)
#             return func  # normally a decorator returns a wrapped function,
#                 # but here we return func unmodified, after registering it
#         return register_fn
#     auto_test.functions = funcs
#     return auto_test
# auto_test = auto_registerer()
