from autotest import auto_test, sampled_from, dictionaries
from hypothesis import given
from hypothesis.strategies import integers, lists
import string


@auto_test(int, int, rv=int)
def get_int(a, b):
    return a


@auto_test(int, int, rv=int)
def sum_2_ints(a, b):
    return a + b


@auto_test(int, int, rv=int)
def divide_2_ints(a, b):
    return a / b


@auto_test(int, str, rv=int)
def sum_int_str(a, b):
    return a + b


@auto_test(int, int, rv=int)
def not_enough_params(a):
    return a


@auto_test(str)
def stringtest(a):
    if not set(a).issubset(set(string.printable)):
        raise ValueError("not printable")


@auto_test([str, int])
def list_strInt(xs):
    if not xs:
        return
    types_in_xs = set([type(x) for x in xs])
    if not types_in_xs.issubset(set([str, unicode, int, long])):
        raise ValueError("wrong types" + ",".join(map(str, types_in_xs)))


@auto_test(set([int, str]))
def set_strInt(xs):
    types_in_xs = set([type(x) for x in xs])
    for x in types_in_xs:
        if x not in [int, str]:
            ValueError("wrong types")
    xs.add(1)
    xs_len = len(xs)
    xs.add(1)
    if not len(xs) == xs_len:
        raise ValueError("not set")


@auto_test(sampled_from([1, 2]))
def sample_test(a):
    if a not in [1, 2]:
        raise ValueError("wrong sample")


@auto_test(dictionaries(
    sampled_from(range(5)),
    sampled_from(["heh", "meh", "peh"]))
)
def dict_example(d):
    if not d:
        return
    keys_types = list(set([type(x) for x in d.keys()]))
    values_types = list(set([type(x) for x in d.values()]))
    if len(keys_types) != 1 and keys_types[0] != int:
        raise ValueError("Invalid keys type")
    if len(values_types) != 1 and values_types[0] != str:
        raise ValueError("Invalid value type")


@given(integers(), integers())
def test_fn_h(a, b):
    return a / b


@given(lists(int), lists(int))
def test_fn2_h(a, b):
    for x in a:
        if a == 0:
            raise ValueError("0 inside")


@given(
    dictionaries(sampled_from(range(5)),
                sampled_from(["heh", "meh", "peh"]))
)
def test_dict(d):
    if not d:
        return
    keys_types = list(set([type(x) for x in d.keys()]))
    values_types = list(set([type(x) for x in d.values()]))
    if len(keys_types) != 1 and keys_types[0] != int:
        raise ValueError("Invalid keys type")
    if len(values_types) != 1 and values_types[0] != str:
        raise ValueError("Invalid value type")

# test_dict()
# test_fn_h()


@auto_test([int, str], int, rv=str)
def list_intStr_int(l, a):
    return ''.join(map(str, l))


@auto_test([bool, int, str], bool, rv=bool)
def list_boolIntStr_bool(a, b):
    return all(a) and b

auto_test.runtests(output="test.log")
